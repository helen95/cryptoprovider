package com.company;

import junit.framework.TestCase;
import org.junit.Test;

import java.io.File;
import java.security.*;
import java.util.Arrays;

public class Check extends TestCase{
    static String res_h_ok = "4cc6ce02c8dc8bd73b6849414ab2e36ccac53d12cab6781655f58fe62eae1e28";
    String path = "src" + File.separator + "com" + File.separator + "company" + File.separator + "test.txt";


    byte[] data = {0x64, 0x6e, 0x6b, 0x73, 0x68, 0x66, 0x6b, 0x6e, 0x64, 0x6b, 0x6a };
    byte[] ethalon =
            {
                    (byte)0x4c, (byte) 0xc6, (byte) 0xce, (byte)0x02, (byte) 0xc8, (byte) 0xdc, (byte)0x8b, (byte)0xd7,
                    (byte)0x3b, (byte)0x68, (byte)0x49, (byte)0x41, (byte)0x4a, (byte)0xb2, (byte)0xe3, (byte)0x6c,
                    (byte)0xca, (byte)0xc5, (byte)0x3d, (byte)0x12, (byte)0xca, (byte)0xb6, (byte)0x78, (byte)0x16,
                    (byte)0x55, (byte)0xf5, (byte)0x8f, (byte)0xe6, (byte)0x2e, (byte)0xae, (byte)0x1e, (byte)0x28};

    public void testGOST() throws Exception{

        MessageDigest gost = MessageDigest.getInstance("GostHash");
        byte[] res_h = gost.digest();
        //for (int i = 0; i < res_h.length; i++)
        //    System.out.format("%02x ", (res_h[i]));

        assertTrue(Arrays.equals(res_h, ethalon));

        System.out.println("Test MAC 3DES - 168 - CBC");
        TripleDes mac = new TripleDes();
        mac.get_mac();
    }


    public static void testEquals(byte[] res, byte[] real) {
        org.junit.Assert.assertEquals(res.length, real.length);
        int i;
        for(i = 0; i < res.length; i++)
            org.junit.Assert.assertEquals(res[i], real[i]);
    }

}
