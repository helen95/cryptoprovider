package com.company;
import javax.crypto.Cipher;
import java.security.SecureRandom;
import java.lang.Math;
import java.math.BigInteger;
import java.util.Base64;

enum Mode {ENCRYPT_MODE, DECRYPT_MODE};

class MyCipher {
    Mode mode;

    void init(Mode mode) {
        this.mode = mode;
    }
}


/**
 * This class is realizing some work with 3DES cipher algorithm
 */
class TripleDes {
    MyCipher ecipher;
    MyCipher dcipher;
    int block_sz = 64;
    int count_cicles_Des = 16;
    int count_Sbloks = 8;
    String k[] = new String[count_cicles_Des]; // KeySize = 48
    String iv = "0000000000111111111100000000001111111111000000000011111111110000";


    int ip[] = {
        58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4,
        62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8,
        57, 49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3,
        61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7
    };

    int ip_rev[] = {
        40,	8,	48,	16,	56,	24,	64,	32,	39,	7,	47,	15,	55,	23,	63,	31,
        38,	6,	46,	14,	54,	22,	62,	30,	37,	5,	45,	13,	53,	21,	61,	29,
        36,	4,	44,	12,	52,	20,	60,	28,	35,	3,	43,	11,	51,	19,	59,	27,
        34,	2,	42,	10,	50,	18,	58,	26,	33,	1,	41,	9,	49,	17,	57,	25,
    };

    int widing[] = {
            32, 1, 2, 3, 4, 5,
            4, 5, 6, 7, 8, 9,
            8, 9, 10, 11, 12, 13,
            12, 13, 14, 15, 16, 17,
            16, 17, 18, 19, 20, 21,
            20, 21, 22, 23, 24, 25,
            24, 25, 26, 27, 28, 29,
            28, 29, 30, 31, 32, 1
    };

    int P[] = {
            16, 7, 20, 21, 29, 12, 28, 17,
            1, 15, 23, 26, 5, 18, 31, 10,
            2, 8, 24, 14, 32, 27, 3, 9,
            19, 13, 30, 6, 22, 11, 4, 25
    };

    /**
     * Constructor
     */
    public TripleDes() {

        ecipher = new MyCipher();
        dcipher = new MyCipher();
        ecipher.init(Mode.ENCRYPT_MODE);
        dcipher.init(Mode.DECRYPT_MODE);
    }



    public String E(String r) {

        String r_new = "";
        for(int i = 0; i < widing.length; i++) {
            r_new += r.charAt(widing[i] - 1);
        }
        return r_new;
    }

    String f(String r, String k) {

        r = E(r);
        String res = "";
        for(int i = 0; i < r.length(); i++) {
            res += (r.charAt(i) ^ k.charAt(i));
        }

        int size_Sbloks = res.length() / count_Sbloks; //6
        String[] B = new String[count_Sbloks];
        String newB = ""; // 4 * count_Sbloks

        for(int i = 0; i < count_Sbloks; i++) {

            B[i] = res.substring(i * size_Sbloks, (i + 1) * size_Sbloks);
            int b_i = Integer.parseInt(B[i], 2);
            int a = (b_i & 1) + 2 * ((b_i & (1 << (size_Sbloks - 1))) >> (size_Sbloks - 1)); // 0 to 3
            b_i >>= 1;
            int b = b_i & ((1 << (size_Sbloks - 2)) - 1); // 0 to 15
            String tmp = Integer.toBinaryString(Sblols.S[i][a * 16 + b]);
            if(tmp.length() < 4) {
                for(int j = 0; j < 4 - tmp.length(); j++) {
                    newB += '0';
                }
            }
            newB += tmp;
        }

        String p_res = "";
        for(int i = 0; i < P.length; i++) {

            p_res += newB.charAt(P[i] - 1);
        }
        return p_res;
    }

    public void getKeys(String k[], String key) {

        String str_k = key;
        int step = 7;
        String s_k = "";
        for(int i = 0; i < str_k.length() / step; i++) { // step=7 -- count of bits, after that we add 1 bit to make it %2 == 1

            int sum = 0;
            for(int j = i * step; j < (i + 1) * step; j++) {

                sum += (int)str_k.charAt(j);
                s_k += str_k.charAt(j);
            }
            s_k += sum % 2 == 1 ? "0" : "1";
        }

        String c[] = new String[count_cicles_Des + 1];
        String d[] = new String[count_cicles_Des + 1];
        for(int i = 0; i < count_cicles_Des + 1; i++) {

            c[i] = "";
            d[i] = "";
        }

        for(int i = 0; i < tables.C0.length; i++) {

            c[0] += s_k.charAt(tables.C0[i] - 1);
            d[0] += s_k.charAt(tables.D0[i] - 1);
        }

        int offset[] = {1, 1, 2, 2,	2, 2, 2, 2,	1, 2, 2, 2,	2, 2, 2, 1};

        for(int i = 1; i <= count_cicles_Des; i++) {
            c[i] = c[i - 1].substring(offset[i - 1]) + c[i - 1].substring(0, offset[i - 1]);
            d[i] = d[i - 1].substring(offset[i - 1]) + d[i - 1].substring(0, offset[i - 1]);
            for(int j = 0; j < tables.K.length; j++) {
                k[i - 1] += (c[i] + d[i]).charAt(tables.K[j] - 1);
            }
        }
    }

    public String Des(String t, String key) {

        String t0 = "";
        for(int i = 0; i < ip.length; i++) {

            t0 += Integer.parseInt(Character.toString(t.charAt(ip[i] - 1)), 2);
        }

        String l[] = new String[count_cicles_Des + 1];
        String r[] = new String[count_cicles_Des + 1];

        for(int i = 0; i < count_cicles_Des; i++) {
            k[i] = "";
        }

        l[0] = t0.substring(0, block_sz / 2);
        r[0] = t0.substring(block_sz / 2, block_sz);
        getKeys(k, key);

        for(int i = 1; i <= count_cicles_Des; i++) {

            l[i] = r[i - 1].substring(0, block_sz / 2);
            String f_res = f(r[i - 1], k[i - 1]);
            String r_i = "";

            for(int j = 0; j < f_res.length(); j++) {

                r_i += (l[i - 1].charAt(j) ^ f_res.charAt(j));
            }
            r[i] = r_i;
        }

        String res = "";
        for(int i = 0; i < ip_rev.length; i++) {

            res += (l[count_cicles_Des] + r[count_cicles_Des]).charAt(ip_rev[i] - 1);
        }
        return res;
    }

    /**
     * encrypt function
     * @param str string of text
     * @return encrypt string
     */
    public String encrypt(String str, String key1, String key2, String key3) {

        int sigma = str.length() % block_sz == 0 ? 0 : 1;
        int count_blocks = str.length() / block_sz + sigma;
        String blocs[] = new String[count_blocks];
        String C[] = new String[count_blocks + 1];
        String new_str = "";
        C[0] = iv;

        for(int i = 0; i < count_blocks; i++) {

            if(str.substring(block_sz * i).length() < block_sz) {
                int l = block_sz - str.substring(block_sz * i).length();
                str += '1';
                for(int j = 0; j < l - 1; j++) {
                    str += '0';
                }
            }
            blocs[i] = str.substring(block_sz * i, (i + 1) * block_sz);
            String tmp = "";
            for(int j = 0; j < blocs[i].length(); j++) {
                tmp += C[i].charAt(j) ^ blocs[i].charAt(j);
            }

            C[i + 1] = Des(Des(Des(tmp, key1), key2), key3);
            new_str += C[i + 1];
        }
        return  new_str;
    }



    public String Des_rev(String t, String key) {

        String t0 = "";
        for(int i = 0; i < ip.length; i++) {
            t0 += Integer.parseInt(Character.toString(t.charAt(ip[i] - 1)), 2);
        };
        String l[] = new String[count_cicles_Des + 1];
        String r[] = new String[count_cicles_Des + 1];

        l[count_cicles_Des] = t0.substring(0, block_sz / 2);
        r[count_cicles_Des] = t0.substring(block_sz / 2, block_sz);
        for(int i = 0; i < count_cicles_Des; i++) {
            k[i] = "";
        }
        getKeys(k, key);

        for(int i = count_cicles_Des; i > 0; i--) {

            r[i - 1] = l[i];
            String f_res = f(l[i], k[i - 1]);
            String r_i = "";

            for(int j = 0; j < f_res.length(); j++) {

                r_i += (r[i].charAt(j) ^ f_res.charAt(j));
            }

            l[i - 1] = r_i;
        }

        String res = "";
        for(int i = 0; i < ip_rev.length; i++) {

            res += (l[0] + r[0]).charAt(ip_rev[i] - 1);
        }
        return res;
    }



    /**
     * decrypt function
     * @param str encrypt string
     * @return decrypt string
     */
    public String decrypt(String str, String key1, String key2, String key3) {

        int count_blocks = str.length() / block_sz;
        String blocs[] = new String[count_blocks];
        String C[] = new String[count_blocks + 1];
        String new_str = "";
        C[0] = iv;

        for(int i = 0; i < count_blocks; i++) {
            if(str.substring(block_sz * i).length() < block_sz) {
                int l = block_sz - str.substring(block_sz * i).length();
                str += '1';
                for(int j = 0; j < l - 1; j++) {
                    str += '0';
                }
            }
            C[i + 1] = str.substring(block_sz * i, (i + 1) * block_sz);
            String tmp = Des_rev(Des_rev(Des_rev(C[i + 1], key3), key2), key1);

            for(int j = 0; j < tmp.length(); j++) {
                new_str += tmp.charAt(j) ^ C[i].charAt(j);
            }
        }
        return  new_str;
    }

    static String generateKey() {
        SecureRandom random = new SecureRandom();
        String key = new BigInteger(56, random).toString(2);
        if(key.length() < 56) {
            int count = 56 - key.length();
            for (int i = 0; i < count; i++) {
                key += new BigInteger(1, random).toString(2);
            }
        }
        return key;
    }

    static int check_res(String a, String b) {

        int flag = 0;
        for(int i = 0; i < a.length(); i++) {
            if(a.charAt(i) != b.charAt(i))
                flag = 1;
        }
        return flag;
    }

    static String getHexStr(String a) {
        String res = "";
        for(int i = 0; i < a.length() / 8; i++) {
            String s = a.substring(i * 8, (i + 1) * 8);
            res += Integer.toHexString(Integer.parseInt(s, 2));
        }
        return res;
    }

    public static void get_mac() {

        String key1 = generateKey();
        /*String key = "hellomydear";//
        String key1 = "";
        for (int i = 0; i < key.length(); i++) {
            key1 += Integer.toBinaryString((int)key.getBytes()[i]);
        }
        System.out.println("key___" + getHexStr(key1));
        */
        String key2 = generateKey();
        String key3 = generateKey();
        TripleDes encrypter = new TripleDes();
        String str = "hello World";
        byte[] str_byte = str.getBytes();
        String bits = "";
        for (int i = 0; i < str_byte.length; i++) {
            bits += Integer.toBinaryString((int)str_byte[i]);
        }
        String eStr = encrypter.encrypt(bits, key1, key2, key3);
        String dStr = encrypter.decrypt(eStr, key1, key2, key3);
        String dStr_new = "";
        int l = dStr.length();
        for(int i = dStr.length() - 1; i >= 0; i--) {
            if(dStr.charAt(i) == '0'){
                l--;
            } else
                break;
        }
        if(l > 0)
            dStr_new = dStr.substring(0, l - 1);
        System.out.println("String__________: " + bits + "\nafter encrypting: " + eStr + "\nafter decrypting: " + dStr_new);
        int res = check_res(bits, dStr);
        if(res == 0)
            System.out.println("OK");
        else
            System.out.println("ERROR");
    }
}
