package com.company;
import java.security.*;


public class CryptoProvider extends Provider {

    private static final String info = "CryptoProvider " + "hash";

    public CryptoProvider() {
        super("CryptoProvider", 1.0, info);
        AccessController.doPrivileged(new java.security.PrivilegedAction() {

            public Object run() {
                put("MessageDigest.GostHash", "com.company.GostHash");
                return null;
            }
        });
    }
}
