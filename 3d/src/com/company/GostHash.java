package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;


/**
 * Основной публичный интерфейс доступа к расчету хеш-суммы ГОСТ 34.11-94.
 */
public class GostHash extends java.security.MessageDigest{
    static String path = "src" + File.separator + "com" + File.separator + "company" + File.separator + "test.txt";

    public GostHash() {
        super("GostHash");
    }

    public void Update(byte[] arr) throws FileNotFoundException, UnsupportedEncodingException {
        File file = new File(path);
        PrintWriter writer = new PrintWriter(path, "UTF-8");
        writer.println(arr);

    }

    @Override
    protected void engineUpdate(byte input) {
        byte [] mas = new byte[1];
        mas[0] = (byte)(input& 0xff);
        try {
            Update(mas);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void engineUpdate(byte[] input, int offset, int len) {
        byte [] mas = new byte[len];
        int i;
        for (i = 0; i < len; i++)
            mas[i] = (byte)(input[i+offset] & 0xff);
        try {
            Update(mas);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected byte[] engineDigest() {
        byte[] input = {0x64, 0x6e, 0x6b, 0x73, 0x68, 0x66, 0x6b, 0x6e, 0x64, 0x6b, 0x6a };

        String res = calcStr(path);
        int l = res.length() % 2 == 0? res.length() / 2 : res.length() / 2 + 1;

        byte[] arr = new byte[l];
        arr = new BigInteger(res,16).toByteArray();
        return arr;
    }

    @Override
    protected void engineReset() {
        File file = new File(path);
    }

    public String hash(){

        String res = calcStr(path);
        return res;
    }

    private static final Object guard = new Object();
    private static Boolean hasNative = Boolean.FALSE;

    public static GostHashIface createImpl() {
        synchronized(guard) {
            if ( hasNative==null ) {
                try {
                    System.loadLibrary("gosthash");
                    hasNative = Boolean.TRUE;
                } catch(UnsatisfiedLinkError x) {
                    hasNative = Boolean.FALSE;
                }
            }
        }
        if ( hasNative.booleanValue() )
            return new GostHashNative();
        return new GostHashJava();
    }

    static final String HEXES = "0123456789abcdef";

    public static String convert(byte[] hash) {
        if ( hash==null || hash.length<32 )
            return null;
        final StringBuilder sb = new StringBuilder();
        for ( int i=0; i<32; ++i ) {
            final byte b = hash[31-i];
            sb.append(HEXES.charAt((b & 0xF0) >> 4))
                    .append(HEXES.charAt((b & 0x0F)));
        }
        return sb.toString();
    }

    public static byte[] calc(java.io.File f) {
        //f = new File(path);
        final GostHashIface ctx = createImpl();
        ctx.init();
        try {
            return ctx.calcHash(f);
        } finally {
            ctx.done();
        }
    }

    public static String calcStr(java.io.File f) {
        final GostHashIface ctx = createImpl();
        ctx.init();
        try {
            return convert(ctx.calcHash(f));
        } finally {
            ctx.done();
        }
    }

    public static String calcStr(String fname) {
        return calcStr(new java.io.File(fname));
    }

    public static byte[] hash(String text) {
        final byte[] block;
        try {
            block = text.getBytes("UTF-8");
        } catch(UnsupportedEncodingException uee) {
            throw new RuntimeException("Сбой при преобразовании строки в блок данных", uee);
        }
        return hash(block);
    }

    public static byte[] hash(byte[] block) {
        final GostHashIface ctx = createImpl();
        ctx.init();
        try {
            ctx.hashBlock(block, 0, block.length);
            return ctx.finishHash();
        } finally {
            ctx.done();
        }
    }

    public static String hashStr(String text) {
        return convert(hash(text));
    }

    public static String hashStr(byte[] block) {
        return convert(hash(block));
    }
}
